﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inaction_warehouses.Application;
using ms_inaction_warehouses.Core.Domain.Entities;

namespace ms_inaction_warehouses.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {

        private readonly IUnitOfWork unitOfWork;

        public LocationsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<Location>>> GetAllAsync(string conn)
        {
            try
            {

                var result = await unitOfWork.Locations.GetAllAsync(conn);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Ubicaciones|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetSearchAsync
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<Location>>> GetSearchAsync(string conn, int start, int limit, string field, string value)
        {
            try
            {
                var result = await unitOfWork.Locations.SearchAsync(conn, start, limit, field, value);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Ubicaciones|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="idLocation"></param>
        /// <returns></returns>
        [HttpGet("{idLocation}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Location>> GetByIdAsync(string conn, long idLocation)
        {
            try
            {
                var result = await unitOfWork.Locations.GetByIdAsync(conn, idLocation);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener la Ubicación|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostAsync(string conn, Location entity)
        {
            try
            {
                var result = await unitOfWork.Locations.AddAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar agregar la Ubicación|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PutAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PutAsync(string conn, Location entity)
        {
            try
            {
                var result = await unitOfWork.Locations.UpdateAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar actualizar la Ubicación|" + ex.Message
                });
            }
        }

        /// <summary>
        /// DeleteAsync Location
        /// </summary>
        /// <param name="idLocation"></param>
        /// <returns></returns>
        [HttpDelete("{idLocation}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteAsync(string conn, long idLocation)
        {
            try
            {
                var result = await unitOfWork.Locations.DeleteAsync(conn, idLocation);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar eliminar la Ubicación|" + ex.Message
                });
            }
        }
    }
}
