﻿using System;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inaction.Core.Init;

namespace ms_inaction_warehouses.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DbController : ControllerBase
    {

        /// <summary>
        /// Patch
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> InitDB(string conn)
        {
            try
            {
                InActionDBInitializer inActionDBInitializer = new InActionDBInitializer(conn);
                await inActionDBInitializer.InitializeDatabaseAsync();
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = true
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar aplicar cambios en la Base de datos|" + ex.Message
                });
            }
        }
    }
}
