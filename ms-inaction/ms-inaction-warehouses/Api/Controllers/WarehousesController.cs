﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inaction_warehouses.Application;
using ms_inaction_warehouses.Core.Domain.Entities;

namespace ms_inaction_warehouses.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WarehousesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public WarehousesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<Warehouse>>> GetAllAsync(string conn)
        {
            try
            {

                var result = await unitOfWork.Warehouses.GetAllAsync(conn);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener los Almacenes|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetSearchAsync
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<Warehouse>>> GetSearchAsync(string conn, int start, int limit, string field, string value)
        {
            try
            {
                var result = await unitOfWork.Warehouses.SearchAsync(conn, start, limit, field, value);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener los Almacenes|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="idWarehouse"></param>
        /// <returns></returns>
        [HttpGet("{idWarehouse}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<Warehouse>> GetByIdAsync(string conn, int idWarehouse)
        {
            try
            {
                var result = await unitOfWork.Warehouses.GetByIdAsync(conn, idWarehouse);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener el Almacen|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PostAsync(string conn, Warehouse entity)
        {
            try
            {
                var result = await unitOfWork.Warehouses.AddAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar agregar el Almacen|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PutAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> PutAsync(string conn, Warehouse entity)
        {
            try
            {
                var result = await unitOfWork.Warehouses.UpdateAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar actualizar el Almacen|" + ex.Message
                });
            }
        }

        /// <summary>
        /// DeleteAsync Warehouse
        /// </summary>
        /// <param name="idWarehouse"></param>
        /// <returns></returns>
        [HttpDelete("{idWarehouse}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteAsync(string conn, int idWarehouse)
        {
            try
            {
                var result = await unitOfWork.Warehouses.DeleteAsync(conn, idWarehouse);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar eliminar el Almacen|" + ex.Message
                });
            }
        }
    }
}
