﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ms_inaction_warehouses.Application;
using ms_inaction_warehouses.Application.Interfaces;
using ms_inaction_warehouses.Infrastructure.Repositories;

namespace ms_inaction_warehouses.Infrastructure
{
    public static class ServiceRegistration
    {
        public static void AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ILocationsRepository, LocationsRepository>();
            services.AddTransient<IWarehousesRepository, WarehousesRepository>();

        }
    }
}
