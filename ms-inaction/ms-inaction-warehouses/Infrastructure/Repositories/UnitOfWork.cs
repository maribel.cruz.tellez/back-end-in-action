﻿using System;
using ms_inaction_warehouses.Application;
using ms_inaction_warehouses.Application.Interfaces;

namespace ms_inaction_warehouses.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(ILocationsRepository locations, IWarehousesRepository warehouses)
        {
            this.Locations = locations;
            this.Warehouses = warehouses;
        }

        public ILocationsRepository Locations { get; }
        public IWarehousesRepository Warehouses { get; }
    }
}
