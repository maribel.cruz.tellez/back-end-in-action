﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using ms_inaction_warehouses.Application.Interfaces;
using ms_inaction_warehouses.Core.Domain.Entities;
using Npgsql;

namespace ms_inaction_warehouses.Infrastructure.Repositories
{
    public class LocationsRepository : ILocationsRepository
    {

        /// <summary>
        /// Get All Locations
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<Location>> GetAllAsync(string connStr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Location>)await conn.QueryAsync<Location>(
                @"SELECT
                    id_location IdLocation
                    ,bin_code BinCode
                FROM ia_locations ");
        }


        /// <summary>
        /// Search Locations
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Location>> SearchAsync(string connStr, int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Location>)await conn.QueryAsync<Location>(
                    String.Format(@"SELECT
                        id_location IdLocation
                        ,bin_code BinCode
                    FROM ia_locations WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                    , new { value });

        }

        /// <summary>
        /// Get Location
        /// </summary>
        /// <param name="idLocation"></param>
        /// <returns></returns>
        public async Task<Location> GetByIdAsync(string connStr, long idLocation)
        {
            using var conn = new NpgsqlConnection(connStr);
            return await conn.QueryFirstOrDefaultAsync<Location>(
                    @"SELECT
                        id_location IdLocation
                        ,bin_code BinCode
                    FROM ia_locations WHERE id_location = @idLocation"
                    , new { idLocation });
        }

        /// <summary>
        /// Add Location
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string connStr, Location entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_locations (
                            bin_code
                            ,disabled
                        )VALUES(
                            @BinCode
                            ,@Disabled)
                        RETURNING id_location"
                        , new { entity.BinCode, entity.Disabled });

            return (id > 0);
        }

        /// <summary>
        /// Update Location
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(string connStr, Location entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_locations
                                SET bin_code = @Name
                                    , disabled = @Disabled
                                WHERE id_location = @IdLocation "
                                , new { entity.BinCode, entity.Disabled, entity.IdLocation });
            return (affected > 0);
        }

        /// <summary>
        /// Delete Location
        /// </summary>
        /// <param name="idLocation"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string connStr, long idLocation)
        {

            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_locations
                            WHERE id_location = @idLocation"
                            , new { idLocation });

            return (affected > 0);
        }
    }
}
