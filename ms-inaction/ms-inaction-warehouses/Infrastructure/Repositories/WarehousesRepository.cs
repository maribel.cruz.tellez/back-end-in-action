﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using ms_inaction_warehouses.Application.Interfaces;
using ms_inaction_warehouses.Core.Domain.Entities;
using Npgsql;

namespace ms_inaction_warehouses.Infrastructure.Repositories
{
    public class WarehousesRepository : IWarehousesRepository
    {

        /// <summary>
        /// Get All Warehouses
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<Warehouse>> GetAllAsync(string connStr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Warehouse>)await conn.QueryAsync<Warehouse>(
                @"SELECT
                    id_customer IdWarehouse
                    ,whs_name   WhsName
                    ,whs_code   WhsCode
                    ,bin_septor BinSeptor
                    ,disabled
                FROM ia_warehouses ");
        }


        /// <summary>
        /// Search Warehouses
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Warehouse>> SearchAsync(string connStr, int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Warehouse>)await conn.QueryAsync<Warehouse>(
                    String.Format(@"SELECT
                        id_customer IdWarehouse
                        ,whs_name   WhsName
                        ,whs_code   WhsCode
                        ,bin_septor BinSeptor
                        ,disabled
                    FROM ia_warehouses WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                    , new { value });

        }

        /// <summary>
        /// Get Warehouse
        /// </summary>
        /// <param name="idWarehouse"></param>
        /// <returns></returns>
        public async Task<Warehouse> GetByIdAsync(string connStr, long idWarehouse)
        {
            using var conn = new NpgsqlConnection(connStr);
            return await conn.QueryFirstOrDefaultAsync<Warehouse>(
                    @"SELECT
                        id_customer IdWarehouse
                        ,whs_name   WhsName
                        ,whs_code   WhsCode
                        ,bin_septor BinSeptor
                        ,disabled
                    FROM ia_warehouses WHERE id_warehouse = @idWarehouse"
                    , new { idWarehouse });
        }

        /// <summary>
        /// Add Warehouse
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string connStr, Warehouse entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_warehouses (
                            whs_name
                            ,whs_code
                            ,bin_septor
                            ,disabled
                        )VALUES(
                            @WhsName
                            ,@WhsCode
                            ,@BinSeptor
                            ,@Disabled)
                        RETURNING id_warehouse"
                        , new { entity.WhsName, entity.WhsCode, entity.BinSeptor, entity.Disabled });

            return (id > 0);
        }

        /// <summary>
        /// Update Warehouse
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(string connStr, Warehouse entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_warehouses
                                SET whs_name = @WhsName
                                    ,whs_code = @WhsCode
                                    ,bin_septor = @BinSeptor
                                    ,disabled = @Disabled
                                WHERE id_warehouse = @IdWarehouse "
                                , new { entity.WhsName, entity.WhsCode, entity.BinSeptor, entity.Disabled, entity.IdWarehouse });
            return (affected > 0);
        }

        /// <summary>
        /// Delete Warehouse
        /// </summary>
        /// <param name="idWarehouse"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string connStr,long idWarehouse)
        {

            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_warehouses
                            WHERE id_warehouse = @idWarehouse"
                            , new { idWarehouse });

            return (affected > 0);
        }
    }
}
