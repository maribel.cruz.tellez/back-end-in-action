﻿using System;
using ms_inaction_warehouses.Application.Interfaces;

namespace ms_inaction_warehouses.Application
{
    public interface IUnitOfWork
    {
        ILocationsRepository Locations { get; }
        IWarehousesRepository Warehouses { get; }
    }
}
