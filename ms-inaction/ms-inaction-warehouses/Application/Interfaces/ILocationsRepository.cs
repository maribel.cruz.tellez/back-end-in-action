﻿using System;
using ms_inaction.Application.Interfaces;
using ms_inaction_warehouses.Core.Domain.Entities;

namespace ms_inaction_warehouses.Application.Interfaces
{
    public interface ILocationsRepository : IInActionRepository<Location>
    {
    }
}
