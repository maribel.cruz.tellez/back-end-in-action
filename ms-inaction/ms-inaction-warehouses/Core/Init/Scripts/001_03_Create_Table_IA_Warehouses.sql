﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	Table ia_warehouses
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_warehouses_id_warehouse_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_warehouses
(
    id_warehouse integer NOT NULL DEFAULT nextval('ia_warehouses_id_warehouse_seq'::regclass),
    whs_code character(50) COLLATE pg_catalog."default" NOT NULL,
    whs_name character(250) COLLATE pg_catalog."default" NOT NULL,
    bin_septor character(10) COLLATE pg_catalog."default" NOT NULL,
    disabled boolean NOT NULL,
    CONSTRAINT ia_warehouses_pkey PRIMARY KEY (id_warehouse),
    CONSTRAINT ia_warehouses_uq_whs_code UNIQUE (whs_code)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;