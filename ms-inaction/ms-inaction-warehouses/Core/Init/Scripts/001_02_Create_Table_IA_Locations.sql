﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	TABLE ia_locations
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_locations_id_location_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_locations
(
    id_location integer NOT NULL DEFAULT nextval('ia_locations_id_location_seq'::regclass),
    bin_code character(20) COLLATE pg_catalog."default" NOT NULL,
    type_location ia_enum_tloc,
    id_warehouse integer NOT NULL,
    sl1_code character(20) COLLATE pg_catalog."default" NOT NULL,
    sl2_code character(20) COLLATE pg_catalog."default" NOT NULL,
    sl3_code character(20) COLLATE pg_catalog."default" NOT NULL,
    max_stock integer NOT NULL,
    min_stock integer NOT NULL,
    alt_sort_cod character(20) COLLATE pg_catalog."default" NOT NULL,
    disabled boolean NOT NULL,
    CONSTRAINT ia_locations_pkey PRIMARY KEY (id_location),
    CONSTRAINT ia_locations_uq_bin_code UNIQUE (bin_code)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;