﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	TYPE ENUM ITEM TYPES LOCATIONS
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

DO $$
BEGIN
 IF NOT EXISTS (
      SELECT 1 FROM pg_type t
      LEFT JOIN pg_namespace p ON t.typnamespace=p.oid
      WHERE t.typname='ia_enum_tloc' AND p.nspname='public'
    ) THEN

    CREATE TYPE public.ia_enum_tloc AS ENUM ('Almacen', 'Empaque', 'Embarque');

END IF;
END
$$;
