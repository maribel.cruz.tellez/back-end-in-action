﻿using System;
namespace ms_inaction_warehouses.Core.Domain.Entities
{
    /// <summary>
    /// Location
    /// </summary>
    public class Location
    {
        public long IdLocation { get; set; }
        public string BinCode { get; set; }
        public string TypeLocation { get; set; }
        public long IdWhs { get; set; }
        public string WhsCode { get; set; }
        public string SL1Code { get; set; }
        public string SL2Code { get; set; }
        public string SL3Code { get; set; }
        public int MaxStock { get; set; }
        public int MinStock { get; set; }
        public string AltSortCod { get; set; }
        public bool Disabled { get; set; }
    }
}
