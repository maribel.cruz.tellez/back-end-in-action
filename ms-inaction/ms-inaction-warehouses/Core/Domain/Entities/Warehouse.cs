﻿using System;
namespace ms_inaction_warehouses.Core.Domain.Entities
{
    /// <summary>
    /// Warehouse
    /// </summary>
    public class Warehouse
    {
        public long IdWarehouse { get; set; }
        public string WhsCode { get; set; }
        public string WhsName { get; set; }
        public string BinSeptor { get; set; }
        public bool Disabled { get; set; }
    }
}
