﻿using System;
using api_gateway_inactionadm.ApiGateway;

namespace api_gateway_inactionadm.Domain
{
    public class Route
    {
        public string Endpoint { get; set; }
        public Destination Destination { get; set; }
    }
}
