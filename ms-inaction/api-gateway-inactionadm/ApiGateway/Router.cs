﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api_gateway_inactionadm.Domain;
using api_gateway_inactionadm.Utils;
using Microsoft.AspNetCore.Http;

namespace api_gateway_inactionadm.ApiGateway
{
    public class Router
    {
        private const string SEPARADOR = "/";
        public List<Route> Routes { get; set; }


        public Router(string routeConfigFilePath)
        {
            Routes routers = JsonLoader.LoadFromFile<Routes>(routeConfigFilePath);

            Routes = routers.RoutesList;
        }

        public async Task<HttpResponseMessage> RouteRequest(HttpRequest request)
        {
            string path = request.Path.ToString();
            string partUri = "";
            Destination destination;

            try
            {
                destination = Routes.First(r => r.Endpoint.Equals(path) || path.StartsWith(r.Endpoint)).Destination;
                if (!destination.Uri.Contains(path))
                {
                    partUri = string.Concat(SEPARADOR, path.Split(SEPARADOR).Last());
                }
            }
            catch
            {
                return ConstructErrorMessage("The path could not be found.");
            }

            return await destination.SendRequest(request, partUri);
        }

        private HttpResponseMessage ConstructErrorMessage(string error)
        {
            HttpResponseMessage errorMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(error)
            };
            return errorMessage;
        }
    }
}
