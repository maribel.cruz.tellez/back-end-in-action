﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace api_gateway_inactionadm.ApiGateway
{
    public class Destination
    {
        public string Uri { get; set; }

        static readonly HttpClient client = new HttpClient();

        public Destination(string uri)
        {
            Uri = uri;
        }

        private Destination()
        {
            Uri = "/";
        }

        public async Task<HttpResponseMessage> SendRequest(HttpRequest request, string partUri)
        {
            string requestContent;
            using (Stream receiveStream = request.Body)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    requestContent = await readStream.ReadToEndAsync();
                }
            }

            using (var newRequest = new HttpRequestMessage(new HttpMethod(request.Method), CreateDestinationUri(request, partUri)))
            {
                newRequest.Content = new StringContent(requestContent, Encoding.UTF8, request.ContentType ?? "application/json");
                return await client.SendAsync(newRequest);
            }
        }

        private string CreateDestinationUri(HttpRequest request, string partUri)
        {
            string destinationUri = string.Concat(Uri, partUri, request.QueryString.ToString());

            return destinationUri;
        }
    }
}
