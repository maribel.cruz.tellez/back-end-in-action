﻿using System;
using System.Collections.Generic;
using api_gateway_inaction.ApiGateway;

namespace api_gateway_inaction.Domain
{
    public class Routes
    {
        public List<Route> RoutesList { get; set; }
        public Destination AuthenticationService { get; set; }
        public Destination ValidatePaymentService { get; set; }
        public Destination GetSocietyService { get; set; }

    }
}
