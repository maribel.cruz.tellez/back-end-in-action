﻿using System;
using api_gateway_inaction.ApiGateway;

namespace api_gateway_inaction.Domain
{
    public class Route
    {
        public string Endpoint { get; set; }
        public Destination Destination { get; set; }
    }
}
