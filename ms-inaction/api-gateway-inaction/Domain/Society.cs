﻿using System;
namespace api_gateway_inaction.Domain
{
    /// <summary>
    /// Society
    /// </summary>
    public class Society
    {
        public long idSociety { get; set; }
        public long idCustomer { get; set; }
        public string name { get; set; }
        public string serverDB { get; set; }
        public string db { get; set; }
        public string userDB { get; set; }
        public string passDB { get; set; }
    }
}
