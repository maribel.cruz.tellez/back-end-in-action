﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using api_gateway_inaction.Domain;
using api_gateway_inaction.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace api_gateway_inaction.ApiGateway
{
    public class Router
    {
        private const string SEPARADOR = "/";

        public List<Route> Routes { get; set; }
        public Destination AuthenticationService { get; set; }
        public Destination ValidatePaymentService { get; set; }
        public Destination GetSocietyService { get; set; }


        public Router(string routeConfigFilePath)
        {
            Routes routers = JsonLoader.LoadFromFile<Routes>(routeConfigFilePath);

            Routes = routers.RoutesList;
            AuthenticationService = routers.AuthenticationService;
            ValidatePaymentService = routers.ValidatePaymentService;
            GetSocietyService = routers.GetSocietyService;
        }
        /// <summary>
        /// RouteRequest
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> RouteRequest(HttpRequest request)
        {
            string path = request.Path.ToString();
            string partUri = "";
            string idSociety;
            Destination destination;
            HttpResponseMessage serviceResponse;

            try
            {
                destination = Routes.First(r => r.Endpoint.Equals(path) || path.StartsWith(r.Endpoint)).Destination;
                if (!destination.Uri.Contains(path))
                {
                    partUri = string.Concat(SEPARADOR, path.Split(SEPARADOR).Last());
                }
            }
            catch
            {
                return ConstructErrorMessage("The path could not be found.");
            }

            //Valid Authentication - Falta desarrollo de servicio
            if (destination.RequiresAuthentication)
            {
                string token = request.Headers["token"];
                request.Query.Append(new KeyValuePair<string, StringValues>("token", new StringValues(token)));
                serviceResponse = await AuthenticationService.SendRequest(request,"");
                if (!serviceResponse.IsSuccessStatusCode) return ConstructErrorMessage("Authentication failed.");
            }
            //Valid PaymentService - Falta desarrollo de servicio
            if (destination.RequiresValidatePaymentService)
            {
                idSociety = request.Headers["idSociety"];
                request.Query.Append(new KeyValuePair<string, StringValues>("idSociety", idSociety));
                serviceResponse = await ValidatePaymentService.SendRequest(request, "");
                if (!serviceResponse.IsSuccessStatusCode) return ConstructErrorMessage("Payment no found.");
            }

            //Ser Conn Society to Request
            try
            {
                request = await SetConnSocietyRequest(request);

            }catch(Exception ex)
            {
                return ConstructErrorMessage(ex.Message);
            }

            return await destination.SendRequest(request, partUri);
        }

        /// <summary>
        /// ConstructErrorMessage
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        private HttpResponseMessage ConstructErrorMessage(string error)
        {
            HttpResponseMessage errorMessage = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NotFound,
                Content = new StringContent(error)
            };
            return errorMessage;
        }

        /// <summary>
        /// SetConnSocietyRequest
        /// </summary>
        /// <param name="request"></param>
        private async Task<HttpRequest> SetConnSocietyRequest(HttpRequest request)
        {
            string conn;
            Society society;
            string idSociety;
            HttpResponseMessage serviceResponse;

            idSociety = request.Headers["idSociety"];
            serviceResponse = await GetSocietyService.SendRequest(request, string.Concat(SEPARADOR, idSociety));
            string societyStrJson = await serviceResponse.Content.ReadAsStringAsync();
            MessageResponse<Society> messageResponse = JsonSerializer.Deserialize<MessageResponse<Society>>(societyStrJson);
            society = messageResponse.data;

            if (society == null)
            {
                throw new ArgumentException("Society no found.");
            }

            conn = string.Format("conn=server={0};Port=5432;Database={1};User Id={2};Password={3};"
                , society.serverDB.Trim(), society.db.Trim(), society.userDB.Trim(), society.passDB).Trim();

            request.QueryString = new QueryString(
                string.Concat(request.QueryString, (string.IsNullOrEmpty(request.QueryString.Value) ? "?" : "&"), conn));

            return request;
        }
    }
}
