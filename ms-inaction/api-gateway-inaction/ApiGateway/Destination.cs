﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace api_gateway_inaction.ApiGateway
{
    public class Destination
    {
        public string Uri { get; set; }
        public bool RequiresAuthentication { get; set; }
        public bool RequiresValidatePaymentService { get; set; }

        static readonly HttpClient client = new HttpClient();

        public Destination(string uri, bool requiresAuthentication, bool requiresValidatePaymentService)
        {
            Uri = uri;
            RequiresAuthentication = requiresAuthentication;
            RequiresValidatePaymentService = requiresValidatePaymentService;
        }

        public Destination(string uri) : this(uri, false, false)
        {
        }

        private Destination()
        {
            Uri = "/";
            RequiresAuthentication = false;
            RequiresValidatePaymentService = false;
        }

        /// <summary>
        /// SendRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="partUri"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SendRequest(HttpRequest request, string partUri)
        {
            string requestContent;
            using (Stream receiveStream = request.Body)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    requestContent = await readStream.ReadToEndAsync();
                }
            }

            using (var newRequest = new HttpRequestMessage(new HttpMethod(request.Method), CreateDestinationUri(request, partUri)))
            {
                newRequest.Content = new StringContent(requestContent, Encoding.UTF8, request.ContentType ?? "application/json");
                return await client.SendAsync(newRequest);
            }
        }

        /// <summary>
        /// CreateDestinationUri
        /// </summary>
        /// <param name="request"></param>
        /// <param name="partUri"></param>
        /// <returns></returns>
        private string CreateDestinationUri(HttpRequest request, string partUri)
        {
            string destinationUri = string.Concat(Uri, partUri, request.QueryString.ToString());

            return destinationUri;
        }
    }
}
