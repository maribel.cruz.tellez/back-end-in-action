﻿using System;
using System.IO;
using System.Text.Json;

namespace api_gateway_inaction.Utils
{
    public class JsonLoader
    {
        public static T LoadFromFile<T>(string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string json = reader.ReadToEnd();
                T result = JsonSerializer.Deserialize<T>(json);
                return result;
            }
        }
    }
}
