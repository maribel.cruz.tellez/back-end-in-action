﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ms_inactionadm.Application.Interfaces
{
    public interface IInActionRepository<T> where T : class
    {
        
        Task<IReadOnlyList<T>> GetAllAsync(string conn);

        Task<T> GetByIdAsync(string conn,long id);

        Task<IReadOnlyList<T>> SearchAsync(string conn,int start, int limit, string field, string value);

        Task<bool> AddAsync(string conn,T entity);

        Task<bool> UpdateAsync(string conn,T entity);

        Task<bool> DeleteAsync(string conn,long id);
    }

}
