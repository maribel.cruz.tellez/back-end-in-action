﻿using System;
using ms_inaction_items.Core.Domain.Entities;
using ms_inactionadm.Application.Interfaces;

namespace ms_inaction_items.Application.Interfaces
{
    public interface IUnitsMsrRepository : IInActionRepository<UnitMsr>
    {
    }
}
