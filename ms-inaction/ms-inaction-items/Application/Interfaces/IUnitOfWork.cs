﻿using System;
using ms_inaction_items.Application.Interfaces;

namespace ms_inaction_items.Application
{
    public interface IUnitOfWork
    {
        IItemsRepository Items { get; }
        IGroupItemsRepository GroupItems { get; }
        IUnitsMsrRepository UnitsMsr { get; }
    }
}
