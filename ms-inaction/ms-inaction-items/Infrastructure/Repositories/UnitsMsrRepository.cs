﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using ms_inaction_items.Application.Interfaces;
using ms_inaction_items.Core.Domain.Entities;
using Npgsql;

namespace ms_inaction_items.Infrastructure.Repositories
{
    public class UnitsMsrRepository : IUnitsMsrRepository
    {
        /// <summary>
        /// Get All UnitsMsr
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<UnitMsr>> GetAllAsync(string connStr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<UnitMsr>)await conn.QueryAsync<UnitMsr>(
                @"SELECT
                    id_unit_msr IdUnitMsr
                    ,name
                    ,description
                FROM ia_units_msr ");
        }


        /// <summary>
        /// Search UnitsMsr
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<UnitMsr>> SearchAsync(string connStr, int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<UnitMsr>)await conn.QueryAsync<UnitMsr>(
                    String.Format(@"SELECT
                        id_unit_msr IdUnitMsr
                        ,name
                        ,description
                    FROM ia_units_msr WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                    , new { value });

        }

        /// <summary>
        /// Get UnitMsr
        /// </summary>
        /// <param name="idUnitMsr"></param>
        /// <returns></returns>
        public async Task<UnitMsr> GetByIdAsync(string connStr, long idUnitMsr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return await conn.QueryFirstOrDefaultAsync<UnitMsr>(
                    @"SELECT
                        id_unit_msr IdUnitMsr
                        ,name
                        ,description
                    FROM ia_units_msr WHERE id_unit_msr = @idUnitMsr"
                    , new { idUnitMsr });
        }

        /// <summary>
        /// Add UnitMsr
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string connStr, UnitMsr entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_units_msr (
                            name
                            ,description
                        )VALUES(
                            @Name
                            ,@Description)
                        RETURNING id_unit_msr"
                        , new { entity.Name, entity.Description });

            return (id > 0);
        }

        /// <summary>
        /// Update UnitMsr
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(string connStr, UnitMsr entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_units_msr
                                SET name = @Name
                                    ,description = @Description
                                WHERE id_unit_msr = @IdUnitMsr "
                                , new { entity.Name, entity.Description, entity.IdUnitMsr });
            return (affected > 0);
        }

        /// <summary>
        /// Delete UnitMsr
        /// </summary>
        /// <param name="idUnitMsr"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string connStr, long idUnitMsr)
        {

            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_units_msr
                            WHERE id_unit_msr = @idUnitMsr"
                            , new { idUnitMsr });

            return (affected > 0);
        }
    }
}
