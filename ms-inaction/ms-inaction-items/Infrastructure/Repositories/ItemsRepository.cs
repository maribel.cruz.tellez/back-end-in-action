﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using ms_inaction_items.Application.Interfaces;
using ms_inaction_items.Core.Domain.Entities;
using Npgsql;

namespace ms_inaction_items.Infrastructure.Repositories
{
    public class ItemsRepository : IItemsRepository
    {

        /// <summary>
        /// Get All Items
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<Item>> GetAllAsync(string connStr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Item>)await conn.QueryAsync<Item>(
                @"SELECT
                     id_item IdItem
                        ,item_name ItemName
                        ,item_code ItemCode
                        ,id_unit_msr IdUnitMsr
                        ,id_grp_itm  IdGrpItm
                        ,type_management TypeManagement
                FROM ia_items ");
        }


        /// <summary>
        /// Search Items
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Item>> SearchAsync(string connStr, int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<Item>)await conn.QueryAsync<Item>(
                    String.Format(@"SELECT
                        id_item IdItem
                        ,item_name ItemName
                        ,item_code ItemCode
                        ,id_unit_msr IdUnitMsr
                        ,id_grp_itm  IdGrpItm
                        ,type_management TypeManagement
                    FROM ia_items WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                    , new { value });

        }

        /// <summary>
        /// Get Item
        /// </summary>
        /// <param name="idItem"></param>
        /// <returns></returns>
        public async Task<Item> GetByIdAsync(string connStr, long idItem)
        {
            using var conn = new NpgsqlConnection(connStr);
            return await conn.QueryFirstOrDefaultAsync<Item>(
                    @"SELECT
                        id_item IdItem
                        ,item_name ItemName
                        ,item_code ItemCode
                        ,id_unit_msr IdUnitMsr
                        ,id_grp_itm  IdGrpItm
                        ,type_management TypeManagement
                    FROM ia_items WHERE id_item = @idItem"
                    , new { idItem });
        }

        /// <summary>
        /// Add Item
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string connStr, Item entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_items (
                            item_name
                            ,item_code
                            ,id_unit_msr
                            ,id_grp_itm
                            ,type_management
                            ,disabled
                        )VALUES(
                            @ItemName
                            ,@ItemCode
                            ,@IdUnitMsr
                            ,@IdGrpItm
                            ,@TypeManagement
                            ,@Disabled)
                        RETURNING id_item"
                        , new { entity.ItemName, entity.ItemCode, entity.IdUnitMsr, entity.IdGrpItm, entity.TypeManagement, entity.Disabled });

            return (id > 0);
        }

        /// <summary>
        /// Update Item
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(string connStr, Item entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_items
                                SET item_name = @ItemName
                                    ,item_code = @ItemCode
                                    ,id_unit_msr = @IdUnitMsr
                                    ,id_grp_itm = @IdGrpItm
                                    ,type_management = @TypeManagement
                                    ,disabled = @Disabled
                                WHERE id_item = @IdItem "
                                , new { entity.ItemName, entity.ItemCode, entity.IdUnitMsr, entity.IdGrpItm, entity.TypeManagement, entity.Disabled, entity.IdItem });
            return (affected > 0);
        }

        /// <summary>
        /// Delete Item
        /// </summary>
        /// <param name="idItem"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string connStr, long idItem)
        {

            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_items
                            WHERE id_item = @idItem"
                            , new { idItem });

            return (affected > 0);
        }
    }
}
