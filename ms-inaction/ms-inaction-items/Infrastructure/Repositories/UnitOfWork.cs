﻿using System;
using ms_inaction_items.Application;
using ms_inaction_items.Application.Interfaces;

namespace ms_inaction_items.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IItemsRepository items, IGroupItemsRepository groupItems, IUnitsMsrRepository unitsMsr)
        {
            this.Items = items;
            this.GroupItems = groupItems;
            this.UnitsMsr = unitsMsr;
        }

        public IItemsRepository Items { get; }
        public IGroupItemsRepository GroupItems { get; }
        public IUnitsMsrRepository UnitsMsr { get; }
    }
}
