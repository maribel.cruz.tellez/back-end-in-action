﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using ms_inaction_items.Application.Interfaces;
using ms_inaction_items.Core.Domain.Entities;
using Npgsql;

namespace ms_inaction_items.Infrastructure.Repositories
{
    public class GroupItemsRepository : IGroupItemsRepository
    {

        /// <summary>
        /// Get All GroupItems
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<GroupItem>> GetAllAsync(string connStr)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<GroupItem>)await conn.QueryAsync<GroupItem>(
                @"SELECT
                    id_group_item IdGroupItem
                    ,name
                    ,description
                FROM ia_group_items ");
        }


        /// <summary>
        /// Search GroupItems
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<GroupItem>> SearchAsync(string connStr, int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(connStr);
            return (List<GroupItem>)await conn.QueryAsync<GroupItem>(
                    String.Format(@"SELECT
                        id_group_item IdGroupItem
                        ,name
                        ,description
                    FROM ia_group_items WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                    , new { value });

        }

        /// <summary>
        /// Get GroupItem
        /// </summary>
        /// <param name="idGroupItem"></param>
        /// <returns></returns>
        public async Task<GroupItem> GetByIdAsync(string connStr,long idGroupItem)
        {
            using var conn = new NpgsqlConnection(connStr);
            return await conn.QueryFirstOrDefaultAsync<GroupItem>(
                    @"SELECT
                        id_group_item IdGroupItem
                        ,name
                        ,description
                    FROM ia_group_items WHERE id_group_item = @idGroupItem"
                    , new { idGroupItem });
        }

        /// <summary>
        /// Add GroupItem
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(string connStr, GroupItem entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_group_items (
                            name
                            ,description
                        )VALUES(
                            @Name
                            ,@Description)
                        RETURNING id_group_item"
                        , new { entity.Name, entity.Description });

            return (id > 0);
        }

        /// <summary>
        /// Update GroupItem
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(string connStr, GroupItem entity)
        {
            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_group_items
                                SET name = @Name
                                    ,description = @Description
                                WHERE id_group_item = @IdGroupItem "
                                , new { entity.Name, entity.Description, entity.IdGroupItem });
            return (affected > 0);
        }

        /// <summary>
        /// Delete GroupItem
        /// </summary>
        /// <param name="idGroupItem"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string connStr, long idGroupItem)
        {

            using var connection = new NpgsqlConnection(connStr);
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_group_items
                            WHERE id_group_item = @idGroupItem"
                            , new { idGroupItem });

            return (affected > 0);
        }
    }
}
