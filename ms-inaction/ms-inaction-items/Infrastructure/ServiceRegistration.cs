﻿using System;
using Microsoft.Extensions.DependencyInjection;
using ms_inaction_items.Application;
using ms_inaction_items.Application.Interfaces;
using ms_inaction_items.Infrastructure.Repositories;

namespace ms_inaction_items.Infrastructure
{
    public static class ServiceRegistration
    {
        public static void AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IItemsRepository, ItemsRepository>();
            services.AddTransient<IGroupItemsRepository, GroupItemsRepository>();
            services.AddTransient<IUnitsMsrRepository, UnitsMsrRepository>();

        }
    }
}
