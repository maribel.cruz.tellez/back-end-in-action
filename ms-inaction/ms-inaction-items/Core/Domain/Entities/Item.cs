﻿using System;
namespace ms_inaction_items.Core.Domain.Entities
{
    /// <summary>
    /// Item
    /// </summary>
    public class Item
    {
        public long IdItem { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string TypeManagement { get; set; }
        public int IdGrpItm { get; set; }
        public string GrpCod { get; set; }
        public int IdUnitMsr { get; set; }
        public string UnitMsr { get; set; }
        public bool Disabled { get; set; }
    }
}
