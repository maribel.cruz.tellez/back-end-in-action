﻿using System;
namespace ms_inaction_items.Core.Domain.Entities
{
    public class UnitMsr
    {
        public long IdUnitMsr { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
