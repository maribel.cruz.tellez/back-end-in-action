﻿using System;
namespace ms_inaction_items.Core.Domain.Entities
{
    /// <summary>
    /// GroupItem
    /// </summary>
    public class GroupItem
    {
        public long IdGroupItem { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
