﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Npgsql;

namespace ms_inaction.Core.Init
{
    public  class InActionDBInitializer
    {
        private readonly string conn;

        public InActionDBInitializer(string conn)
        {
            this.conn = conn;
        }

        public async Task InitializeDatabaseAsync()
        {
            try
            {
                string data;
                string[] fileEntries = Directory.GetFiles("Core/Init/Scripts");
                List<string> sortAscendingFileEntries = new List<string>(fileEntries);
                sortAscendingFileEntries.Sort();

                using (var connection = new NpgsqlConnection(conn))
                {
                    connection.Open();

                    NpgsqlCommand comm = connection.CreateCommand();

                    foreach (string file in sortAscendingFileEntries)
                    {
                        data = File.ReadAllText(file);

                        comm.CommandText = data;
                        await comm.ExecuteNonQueryAsync();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
    }
}
