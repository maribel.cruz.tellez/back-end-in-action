﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	TABLE ia_items
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_items_id_item_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_items
(
    id_item integer NOT NULL DEFAULT nextval('ia_items_id_item_seq'::regclass),
    item_code character(20) COLLATE pg_catalog."default" NOT NULL,
    item_name character(150) COLLATE pg_catalog."default" NOT NULL,
    id_unit_msr integer NOT NULL,
    id_group_item integer NOT NULL,
    disabled boolean NOT NULL,
    type_management ia_enum_itm,
    CONSTRAINT ia_items_pkey PRIMARY KEY (id_item),
    CONSTRAINT ia_items_uq_item_code UNIQUE (item_code),
    CONSTRAINT ia_items_fk_id_unit_msr FOREIGN KEY (id_unit_msr)
        REFERENCES public.ia_units_msr (id_unit_msr) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT ia_items_fk_id_group_item FOREIGN KEY (id_group_item)
        REFERENCES public.ia_group_items (id_group_item) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;