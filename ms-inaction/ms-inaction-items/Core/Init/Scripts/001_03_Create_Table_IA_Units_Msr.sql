﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	Table ia_units_msr
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_units_msr_id_unit_msr_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_units_msr
(
    id_unit_msr integer NOT NULL DEFAULT nextval('ia_units_msr_id_unit_msr_seq'::regclass),
    name character(50) COLLATE pg_catalog."default" NOT NULL,
    description character(250) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ia_units_msr_pkey PRIMARY KEY (id_unit_msr),
    CONSTRAINT ia_units_msr_uq_name UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;