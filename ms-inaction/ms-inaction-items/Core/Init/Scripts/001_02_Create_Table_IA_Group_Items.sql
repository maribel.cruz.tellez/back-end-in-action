﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	Table ia_group_items
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_group_items_id_group_item_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_group_items
(
    id_group_item integer NOT NULL DEFAULT nextval('ia_group_items_id_group_item_seq'::regclass),
    name character(50) COLLATE pg_catalog."default" NOT NULL,
    description character(250) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ia_group_items_pkey PRIMARY KEY (id_group_item),
    CONSTRAINT ia_group_items_uq_name UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;