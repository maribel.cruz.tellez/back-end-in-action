﻿using System;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inaction_items.Application;
using ms_inaction_items.Core.Domain.Entities;

namespace ms_inaction_items.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupItemsController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public GroupItemsController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetAllAsync(string conn)
        {
            try
            {

                var result = await unitOfWork.GroupItems.GetAllAsync(conn);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener los Grupos de articulos|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetSearchAsync
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetSearchAsync(string conn, int start, int limit, string field, string value)
        {
            try
            {
                var result = await unitOfWork.GroupItems.SearchAsync(conn, start, limit, field, value);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener los Grupos de articulos|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="idGroupItem"></param>
        /// <returns></returns>
        [HttpGet("{idGroupItem}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetByIdAsync(string conn, int idGroupItem)
        {
            try
            {
                var result = await unitOfWork.GroupItems.GetByIdAsync(conn, idGroupItem);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener el Grupo de articulos|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PostAsync(string conn, GroupItem entity)
        {
            try
            {
                var result = await unitOfWork.GroupItems.AddAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar agregar el Grupo de articulos|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PutAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PutAsync(string conn, GroupItem entity)
        {
            try
            {
                var result = await unitOfWork.GroupItems.UpdateAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar actualizar el Grupo de articulos|" + ex.Message
                });
            }
        }

        /// <summary>
        /// DeleteAsync GroupItem
        /// </summary>
        /// <param name="idGroupItem"></param>
        /// <returns></returns>
        [HttpDelete("{idGroupItem}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> DeleteAsync(string conn, int idGroupItem)
        {
            try
            {
                var result = await unitOfWork.GroupItems.DeleteAsync(conn, idGroupItem);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar eliminar el Grupo de articulos|" + ex.Message
                });
            }
        }

    }
}
