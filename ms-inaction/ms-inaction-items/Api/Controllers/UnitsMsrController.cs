﻿using System;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inaction_items.Application;
using ms_inaction_items.Core.Domain.Entities;

namespace ms_inaction_items.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UnitsMsrController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public UnitsMsrController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetAllAsync(string conn)
        {
            try
            {

                var result = await unitOfWork.UnitsMsr.GetAllAsync(conn);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Unidades de medida|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetSearchAsync
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetSearchAsync(string conn, int start, int limit, string field, string value)
        {
            try
            {
                var result = await unitOfWork.UnitsMsr.SearchAsync(conn, start, limit, field, value);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Unidades de medida|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="idUnitMsr"></param>
        /// <returns></returns>
        [HttpGet("{idUnitMsr}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetByIdAsync(string conn, int idUnitMsr)
        {
            try
            {
                var result = await unitOfWork.UnitsMsr.GetByIdAsync(conn, idUnitMsr);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener la Unidad de medida|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PostAsync(string conn, UnitMsr entity)
        {
            try
            {
                var result = await unitOfWork.UnitsMsr.AddAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar agregar la Unidad de medida|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PutAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PutAsync(string conn, UnitMsr entity)
        {
            try
            {
                var result = await unitOfWork.UnitsMsr.UpdateAsync(conn, entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar actualizar la Unidad de medida|" + ex.Message
                });
            }
        }

        /// <summary>
        /// DeleteAsync UnitMsr
        /// </summary>
        /// <param name="idUnitMsr"></param>
        /// <returns></returns>
        [HttpDelete("{idUnitMsr}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> DeleteAsync(string conn, int idUnitMsr)
        {
            try
            {
                var result = await unitOfWork.UnitsMsr.DeleteAsync(conn, idUnitMsr);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar eliminar la Unidad de medida|" + ex.Message
                });
            }
        }
    }
}
