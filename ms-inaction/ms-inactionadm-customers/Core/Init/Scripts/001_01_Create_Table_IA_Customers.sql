﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	Table ia_customers
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_customers_id_customer_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_customers
(
    id_customer integer NOT NULL DEFAULT nextval('ia_customers_id_customer_seq'::regclass),
    name character(200) COLLATE pg_catalog."default" NOT NULL,
    disabled boolean NOT NULL,
    CONSTRAINT ia_customers_pkey PRIMARY KEY (id_customer),
    CONSTRAINT ia_customers_uq_name UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;