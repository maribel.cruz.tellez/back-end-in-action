﻿-- =============================================
-- Autor: Maribel Cruz Téllez
-- Fecha Creación: 26/08/2021
-- Descripción:	Table ia_societies
-- History:
-- <dd/mm/yyyy> <dev> <description>
-- =============================================

CREATE SEQUENCE IF NOT EXISTS public.ia_societies_id_society_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE IF NOT EXISTS public.ia_societies
(
    id_society integer NOT NULL DEFAULT nextval('ia_societies_id_society_seq'::regclass),
    name character(250) COLLATE pg_catalog."default" NOT NULL,
    server_db character(200) COLLATE pg_catalog."default" NOT NULL,
    db character(20) COLLATE pg_catalog."default" NOT NULL,
    user_db character(20) COLLATE pg_catalog."default" NOT NULL,
    pass_db character(20) COLLATE pg_catalog."default" NOT NULL,
    disabled boolean NOT NULL,
    id_customer integer NOT NULL,
    CONSTRAINT ia_societies_pkey PRIMARY KEY (id_society),
    CONSTRAINT ia_societies_uq_name UNIQUE (name),
    CONSTRAINT ia_societies_uq_db UNIQUE (db),
    CONSTRAINT fk_cus_soc FOREIGN KEY (id_customer)
        REFERENCES public.ia_customers (id_customer) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;