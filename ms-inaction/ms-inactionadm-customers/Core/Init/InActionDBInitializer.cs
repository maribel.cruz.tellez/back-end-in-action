﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ms_inaction.Core.Common;
using Npgsql;

namespace ms_inactionadm.Core.Init
{
    public  class InActionDBInitializer
    {
        private readonly IConfiguration configuration;

        public InActionDBInitializer(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        public async Task InitializeDatabaseAsync()
        {
            try
            {
                string data;
                string[] fileEntries = Directory.GetFiles("Core/Init/Scripts");
                List<string> sortAscendingFileEntries = new List<string>(fileEntries);
                sortAscendingFileEntries.Sort();

                using (var connection = new NpgsqlConnection(InActionTools.GetConn(configuration)))
                {
                    connection.Open();

                    NpgsqlCommand comm = connection.CreateCommand();

                    foreach (string file in sortAscendingFileEntries)
                    {
                        data = File.ReadAllText(file);

                        comm.CommandText = data;
                        await comm.ExecuteNonQueryAsync();
                    }
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.ToString());
                //Generar log
            }

        }
    }
}
