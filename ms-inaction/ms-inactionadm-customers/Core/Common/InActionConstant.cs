﻿using System;
namespace ms_inactiona.Core.Common
{
    public static class InActionConstant
    {
        /// <summary>
        /// IA Services
        /// </summary>
        public const string IA_Services_ItemsService = "itemsService";
        public const string IA_Services_WarehousesService = "warehousesService";

        /// <summary>
        /// IA Services Uri
        /// </summary>
        public const string IA_Services_Uri_ItemsService = "api/items";
        public const string IA_Services_Uri_WarehousesService = "api/warehouses";

        /// <summary>
        /// IA Env
        /// </summary>
        public const string IA_Env_ItemsService = "IA_SERVICE_ITEMS";
        public const string IA_Env_WarehousesService = "IA_SERVICE_WAREHOUSES";
    }
}
