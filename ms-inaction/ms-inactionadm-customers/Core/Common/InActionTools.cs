﻿using System;
using Microsoft.Extensions.Configuration;

namespace ms_inaction.Core.Common
{
    public static class InActionTools
    {
        public static string GetConn(IConfiguration configuration)
        {
            return configuration.GetValue<string>("IA_ADM_DB_CONN");
        }
    }
}
