﻿namespace ms_inactionadm_customers.Core.Domain.Entities
{
    /// <summary>
    /// Customer
    /// </summary>
    public class Customer
    {
        public long IdCustomer { get; set; }
        public string Name { get; set; }
        public bool Disabled { get; set; }
    }
}
