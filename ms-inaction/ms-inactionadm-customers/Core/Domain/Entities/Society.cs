﻿using System;
namespace ms_inactionadm_customers.Core.Domain.Entities
{
    /// <summary>
    /// Society
    /// </summary>
    public class Society
    {
        public long IdSociety { get; set; }
        public long IdCustomer { get; set; }
        public string Name { get; set; }
        public string ServerDB { get; set; }
        public string DB { get; set; }
        public string UserDB { get; set; }
        public string PassDB { get; set; }
        public bool Disabled { get; set; }
    }
}
