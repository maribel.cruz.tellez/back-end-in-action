﻿using System;
namespace ms_inaction.Core.Domain
{
    public class MessageResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
