﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ms_inactionadm.Core.Init;

namespace ms_inactionadm_customers.Api
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            IWebHost webHost = CreateWebHostBuilder(args).Build();

            var config = webHost.Services.GetRequiredService<IConfiguration>();

            //Init db for Customers
            InActionDBInitializer inActionDBInitializer = new InActionDBInitializer(config);
            await inActionDBInitializer.InitializeDatabaseAsync();

            // Run the WebHost, and start accepting requests
            // There's an async overload, so we may as well use it
            await webHost.RunAsync();

        }
            public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
                WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>();
        
    }
}
