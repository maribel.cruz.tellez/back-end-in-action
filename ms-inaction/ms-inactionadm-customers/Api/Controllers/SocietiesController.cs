﻿using System;
using System.Net;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ms_inaction.Core.Domain;
using ms_inactionadm.Application;
using ms_inactionadm_customers.Core.Domain.Entities;

namespace ms_inactionadm_customers.Api.Controllers
{
    /// <summary>
    /// Microservice Customers
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class SocietiesController : ControllerBase
    {
        private readonly IUnitOfWork unitOfWork;

        public SocietiesController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        /// <summary>
        /// GetAllAsync
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetAllAsync()
        {
            try
            {

                var result = await unitOfWork.Societies.GetAllAsync();
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Sociedades|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetSearchAsync
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("search")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetSearchAsync(int start, int limit, string field, string value)
        {
            try
            {
                var result = await unitOfWork.Societies.SearchAsync(start, limit, field, value);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener las Sociedades|" + ex.Message
                });
            }
        }

        /// <summary>
        /// GetByIdAsync
        /// </summary>
        /// <param name="idSociety"></param>
        /// <returns></returns>
        [HttpGet("{idSociety}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> GetByIdAsync(int idSociety)
        {
            try
            {
                var result = await unitOfWork.Societies.GetByIdAsync(idSociety);
                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar obtener la Sociedad|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PostAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PostAsync(Society entity)
        {
            try
            {
                var result = await unitOfWork.Societies.AddAsync(entity);

                await unitOfWork.Items.InitDB(string.Format("server={0};Port=5432;Database={1};User Id={2};Password={3};"
                , entity.ServerDB, entity.DB, entity.UserDB, entity.PassDB));

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar agregar la Sociedad|" + ex.Message
                });
            }
        }

        /// <summary>
        /// PutAsync
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> PutAsync(Society entity)
        {
            try
            {
                var result = await unitOfWork.Societies.UpdateAsync(entity);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar actualizar la Sociedad|" + ex.Message
                });
            }
        }

        /// <summary>
        /// DeleteAsync Society
        /// </summary>
        /// <param name="idSociety"></param>
        /// <returns></returns>
        [HttpDelete("{idSociety}")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(MessageResponse), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<MessageResponse>> DeleteAsync(int idSociety)
        {
            try
            {
                var result = await unitOfWork.Societies.DeleteAsync(idSociety);

                return Ok(new MessageResponse
                {
                    Success = true,
                    Data = result
                });
            }
            catch (Exception ex)
            {
                return Ok(new MessageResponse
                {
                    Success = false,
                    Message = "Ocurrió un error al intentar eliminar la Sociedad|" + ex.Message
                });
            }
        }
    }
}
