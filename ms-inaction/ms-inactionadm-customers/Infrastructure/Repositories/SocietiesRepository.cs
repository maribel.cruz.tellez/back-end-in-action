﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ms_inaction.Core.Common;
using ms_inactionadm_customers.Application.Interfaces;
using ms_inactionadm_customers.Core.Domain.Entities;
using Npgsql;

namespace ms_inactionadm_customers.Infrastructure.Repositories
{
    public class SocietiesRepository :ISocietiesRepository
    {
        private readonly IConfiguration configuration;

        public SocietiesRepository(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        /// <summary>
        /// Get All Societies
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<Society>> GetAllAsync()
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
            return (IReadOnlyList<Society>)await conn.QueryAsync<Society>(
                        @"SELECT
                            id_society IdSociety
                            ,name
                            ,server_db ServerDB
                            ,db
                            ,user_db UserDB
                            ,pass_db PassDB
                            ,id_customer IdCustomer
                        FROM ia_societies ");
        }

        /// <summary>
        /// Search Societies
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Society>> SearchAsync(int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
            return (List<Society>)await conn.QueryAsync<Society>(
                        String.Format(@"SELECT
                            id_society IdSociety
                            ,name
                            ,server_db ServerDB
                            ,db
                            ,user_db UserDB
                            ,pass_db PassDB
                            ,id_customer IdCustomer
                        FROM ia_societies WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field, limit, start)
                        , new { value });
        }

        /// <summary>
        /// Get Society
        /// </summary>
        /// <param name="idSociety"></param>
        /// <returns></returns>
        public async Task<Society> GetByIdAsync(long idSociety)
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
            return await conn.QueryFirstOrDefaultAsync<Society>(
                        @"SELECT
                            id_society IdSociety
                            ,name
                            ,server_db ServerDB
                            ,db
                            ,user_db UserDB
                            ,pass_db PassDB
                            ,id_customer IdCustomer
                        FROM ia_societies
                        WHERE id_society = @idSociety"
                        , new { idSociety });

        }

        /// <summary>
        ///  Add Society
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(Society entity)
        {
            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));
            var id = await connection.ExecuteScalarAsync<int>(
                            @"INSERT INTO ia_societies (
                                name
                                ,server_db
                                ,db
                                ,user_db
                                ,pass_db
                                ,id_customer
                                ,disabled
                            )VALUES(
                                @Name
                                ,@ServerDB
                                ,@DB
                                ,@UserDB
                                ,@PassDB
                                ,@IdCustomer
                                ,@Disabled)
                            RETURNING id_society"
                            , new {entity.Name, entity.ServerDB, entity.DB, entity.UserDB, entity.PassDB, entity.IdCustomer, entity.Disabled });

            await connection.ExecuteAsync(
                String.Format("CREATE DATABASE \"{0}\" " +
                         @"WITH
                        OWNER = {1}
                        ENCODING = 'UTF8'
                        LC_COLLATE = 'en_US.UTF-8'
                        LC_CTYPE = 'en_US.UTF-8'
                        CONNECTION LIMIT = -1; ", entity.DB, entity.UserDB)
                , new { entity.DB, entity.UserDB });
            return (id > 0);
        }


        /// <summary>
        /// Update Society
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(Society entity)
        {
            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));
            var affected = await connection.ExecuteAsync(
                            @"UPDATE ia_societies
                                SET name = @Name
                                , server_db = @ServerDB
                                , user_db = @UserDB
                                , pass_db = @PassDB
                                , disabled = @Disabled
                            WHERE id_society = @IdSociety "
                            , new { entity.Name, entity.ServerDB, entity.DB, entity.UserDB, entity.PassDB, entity.IdCustomer, entity.Disabled, entity.IdSociety });

            return (affected > 0);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSociety"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(long idSociety)
        {
            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));

            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_societies
                            WHERE id_society = @idSociety"
                            , new { idSociety });

            return (affected > 0);
        }
    }
}
