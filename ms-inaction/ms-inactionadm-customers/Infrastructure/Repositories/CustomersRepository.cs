﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using ms_inaction.Core.Common;
using ms_inactionadm_customers.Application.Interfaces;
using ms_inactionadm_customers.Core.Domain.Entities;
using Npgsql;

namespace ms_inactionadm_customers.Infrastructure.Repositories
{
    public class CustomersRepository : ICustomersRepository
    {
        private readonly IConfiguration configuration;

        public CustomersRepository(IConfiguration configuration)
        {
            this.configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        /// <summary>
        /// Get All Customers
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyList<Customer>> GetAllAsync()
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
            return (List<Customer>)await conn.QueryAsync<Customer>(
                @"SELECT
                    id_customer IdCustomer
                    ,name
                FROM ia_customers ");
        }


        /// <summary>
        /// Search Customers
        /// </summary>
        /// <param name="start"></param>
        /// <param name="limit"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IReadOnlyList<Customer>> SearchAsync(int start, int limit, string field, string value)
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
                return (List<Customer>)await conn.QueryAsync<Customer>(
                        String.Format(@"SELECT
                        id_customer IdCustomer
                        ,name
                    FROM ia_customers WHERE {0} = @value  LIMIT {1} OFFSET {2} ", field,limit,start)
                        , new { value });
            
        }

        /// <summary>
        /// Get Customer
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<Customer> GetByIdAsync(long idCustomer)
        {
            using var conn = new NpgsqlConnection(InActionTools.GetConn(configuration));
            return await conn.QueryFirstOrDefaultAsync<Customer>(
                    @"SELECT
                        id_customer IdCustomer
                        ,name
                    FROM ia_customers WHERE id_customer = @idCustomer"
                    , new { idCustomer });
        }

        /// <summary>
        /// Add Customer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(Customer entity)
        {
            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));
            var id = await connection.ExecuteScalarAsync<int>(
                        @"INSERT INTO ia_customers (
                            name
                            ,disabled
                        )VALUES(
                            @Name
                            ,@Disabled)
                        RETURNING id_customer"
                        , new { entity.Name, entity.Disabled });

            return (id > 0);
        }

        /// <summary>
        /// Update Customer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> UpdateAsync(Customer entity)
        {
            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));
            var affected = await connection.ExecuteAsync(
                                @"UPDATE ia_customers
                                SET name = @Name
                                    , disabled = @Disabled
                                WHERE id_customer = @IdCustomer "
                                , new { entity.Name, entity.Disabled, entity.IdCustomer });
            return (affected > 0);
        }

        /// <summary>
        /// Delete Customer
        /// </summary>
        /// <param name="idCustomer"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(long idCustomer)
        {

            using var connection = new NpgsqlConnection(InActionTools.GetConn(configuration));
            var affected = await connection.ExecuteAsync(
                            @"DELETE FROM ia_customers
                            WHERE id_customer = @idCustomer"
                            , new { idCustomer });

            return (affected > 0);
        }


    }
}
    