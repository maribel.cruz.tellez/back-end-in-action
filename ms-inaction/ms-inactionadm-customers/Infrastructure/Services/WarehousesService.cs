﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ms_inaction.Core.Domain;
using ms_inactiona.Core.Common;
using ms_inactionadm_customers.Application.Interfaces;
using Newtonsoft.Json;

namespace ms_inactionadm_customers.Infrastructure.Services
{
    public class WarehousesService : IWarehousesService
    {
        private readonly IHttpClientFactory httpClientFactory;

        public WarehousesService(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public async Task<MessageResponse> InitDB(string conn)
        {

            MessageResponse messageResponse = new MessageResponse();

            var values = new Dictionary<string, string>();
            values.Add("conn", conn);
            var content = new FormUrlEncodedContent(values);

            var client = httpClientFactory.CreateClient(InActionConstant.IA_Services_WarehousesService);
            var response = await client.PatchAsync(InActionConstant.IA_Services_Uri_WarehousesService, content);

            if (response.IsSuccessStatusCode)
            {
                var constent = await response.Content.ReadAsStringAsync();

                messageResponse = JsonConvert.DeserializeObject<MessageResponse>(constent);
            }
            return messageResponse;
        }
    }
}
