﻿using System;
using ms_inactionadm.Application;
using ms_inactionadm_customers.Application.Interfaces;

namespace ms_inactionadm_customers.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(ICustomersRepository customers, ISocietiesRepository societies, IWarehousesService warehouses, IItemsService items)
        {
            this.Customers = customers;
            this.Societies = societies;
            this.Warehouses = warehouses;
            this.Items = items;
        }

        /// <summary>
        /// Repositories
        /// </summary>
        public ICustomersRepository Customers { get; }
        public ISocietiesRepository Societies { get; }

        /// <summary>
        /// Services
        /// </summary>
        public IItemsService Items { get; }
        public IWarehousesService Warehouses { get; }


    }
}
