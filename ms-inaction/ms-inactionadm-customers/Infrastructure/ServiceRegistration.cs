﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ms_inactiona.Core.Common;
using ms_inactionadm.Application;
using ms_inactionadm_customers.Application.Interfaces;
using ms_inactionadm_customers.Infrastructure.Repositories;
using ms_inactionadm_customers.Infrastructure.Services;

namespace ms_inactionadm_customers.Infrastructure
{
    public static class ServiceRegistration
    {

        public static void AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            //Repositories
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<ICustomersRepository, CustomersRepository>();
            services.AddTransient<ISocietiesRepository, SocietiesRepository>();

            services.AddTransient<IItemsService, ItemsService>();
            services.AddTransient<IWarehousesService, WarehousesService>();

            //HttpClient
            services.AddHttpClient(InActionConstant.IA_Services_ItemsService, c =>
            {
                c.BaseAddress = new Uri(configuration[InActionConstant.IA_Env_ItemsService]);
            });
            services.AddHttpClient(InActionConstant.IA_Services_WarehousesService, c =>
            {
                c.BaseAddress = new Uri(configuration[InActionConstant.IA_Env_WarehousesService]);
            });

        }
    }
}

