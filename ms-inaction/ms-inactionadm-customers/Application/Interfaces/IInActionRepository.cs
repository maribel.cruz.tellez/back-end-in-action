﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ms_inaction.Application.Interfaces
{
    public interface IInActionRepository<T> where T : class
    {
        
        Task<IReadOnlyList<T>> GetAllAsync();

        Task<T> GetByIdAsync(long id);

        Task<IReadOnlyList<T>> SearchAsync(int start, int limit, string field, string value);

        Task<bool> AddAsync(T entity);

        Task<bool> UpdateAsync(T entity);

        Task<bool> DeleteAsync(long id);
    }

}
