﻿using System;
using ms_inactionadm_customers.Application.Interfaces;

namespace ms_inactionadm.Application
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Repositories
        /// </summary>
        ICustomersRepository Customers { get; }
        ISocietiesRepository Societies { get; }

        /// <summary>
        /// Services
        /// </summary>
        IItemsService Items { get; }
        IWarehousesService Warehouses { get; }
    } 
}
