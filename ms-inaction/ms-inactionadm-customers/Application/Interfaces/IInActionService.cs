﻿using System;
using System.Threading.Tasks;
using ms_inaction.Core.Domain;

namespace ms_inactionadm_customers.Application.Interfaces
{
    public interface IInActionService
    {
        public Task<MessageResponse> InitDB(string conn);
    }
}
