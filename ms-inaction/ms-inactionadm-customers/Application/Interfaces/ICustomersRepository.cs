﻿using ms_inaction.Application.Interfaces;
using ms_inactionadm_customers.Core.Domain.Entities;

namespace ms_inactionadm_customers.Application.Interfaces
{
    public interface ICustomersRepository : IInActionRepository<Customer>
    {

    }
}
